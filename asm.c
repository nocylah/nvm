#include "nvm.h"

enum TokenType {
    NONE,
    STRING,
    INTEGER,
    OPCODE,
    LABEL,
    REF_ADDR, REF_SIZE, REF_LOAD, REF_STORE, REF_USE,
    MACRO,
    DATA,
    COMMENT
};

struct Token {
    enum TokenType type;
    enum OpCode opcode;
    enum Type inttype;
    enum Stack stack;
    unsigned long long integer;
    char* str;

    size_t offset;
    size_t size;
    struct Token* ref;
    struct Token* macro;

    struct Token* next;
};

void token_free(struct Token* tok) {
    struct Token* last = tok;
    while (tok != NULL) {
        if (tok->macro != NULL) token_free(tok->macro);
        if (tok->str != NULL) free(tok->str);
        last = tok;
        tok = tok->next;

        free(last);
    }
}

struct Token* token_next(struct Token* prev, enum TokenType type) {
    struct Token** next = prev == NULL ? &prev : &prev->next;
    *next = calloc(1, sizeof(struct Token));
    (*next)->type = type;
    return *next;
}

struct Token* token_find(struct Token* tok, enum TokenType type, const char* str) {
    for (; tok != NULL; tok = tok->next) {
        if (tok->type == type && strcmp(tok->str, str) == 0) return tok;
    }
    return NULL;
}

struct Token* token_copy(struct Token* tok) {
    struct Token* copy = calloc(1, sizeof(struct Token));
    *copy = *tok;
    if (tok->str != NULL) {
        copy->str = calloc(strlen(tok->str) + 1, 1);
        strcpy(copy->str, tok->str);
    }

    return copy;
}

struct Token* token_insert(struct Token* src, struct Token* dst, struct Token* last) {
    while (src != NULL) {
        dst->next = token_copy(src);
        dst = dst->next;

        src = src->next;
    }
    dst->next = last;
    return dst;
}

void get_loc(FILE* fin, unsigned* line, unsigned* chr) {
    *line = 1; *chr = 1;
    long loc = ftell(fin);
    rewind(fin);
    while (loc > 0) {
        char c = fgetc(fin);
        if (c == '\n') {
            *line = *line + 1;
            *chr = 1;
        }

        *chr = *chr + 1;
        loc--;
    }
}

void error(FILE* fin, const char* msg) {
    if (fin == NULL) {
        fprintf(stderr, "ERROR: %s\n", msg);
    } else {
        unsigned line, chr;
        get_loc(fin, &line, &chr);
        fprintf(stderr, "ERROR (%d,%d): %s\n", line, chr, msg);
    }
    exit(1);
}

void check_scanf(FILE* fin, const char* err_str, int ret) {
    if (ret == EOF || ret == 0) error(fin, err_str);
}

uint8_t gen_instr(struct Token* tok) {
    return (tok->inttype << 6) | (tok->stack << 5) | tok->opcode;
}

enum Type parse_inttype(FILE* fin) {
    assert(fgetc(fin) == '/');
    char buf[64];
    check_scanf(fin, "unexpected token", fscanf(fin, "%[us0-9]", buf));

    for (size_t i = 0; i <= S32; i++) {
        if (strcmp(TypeStr[i], buf) == 0) {
            return i;
        }
    }

    char msg[64];
    snprintf(msg, 64, "bad int size '%s'", buf);
    error(fin, msg); return 0;
}

enum OpCode parse_opcode(FILE* fin) {
    char buf[64];
    check_scanf(fin, "bad opcode", fscanf(fin, "%[A-Z]", buf));

    for (size_t i = 0; i <= NOP; i++) {
        if (strcmp(OpCodeStr[i], buf) == 0) {
            return i;
        }
    }

    char msg[64];
    snprintf(msg, 64, "bad opcode '%s'", buf);
    error(fin, msg); return 0;
}

int fpeekc(FILE* fin) {
    int c = fgetc(fin);
    ungetc(c, fin);
    return c;
}

#define LABEL_FMT "%m[a-zA-Z0-9_.-]"

void parse(FILE* fin, struct Token* tok) {
    struct Token* macro = NULL;
    int c;
    char buf[128];

    while ((c = fgetc(fin)) != EOF) {
        // whitespace
        if (c == ' ' || c == '\t' || c == '\n' || c == '\r') {}
        // ignore
        else if (c == '(' || c == ')') {}
        // comments
        else if (c == ';') {
            tok = token_next(tok, COMMENT);
            check_scanf(fin, "weird comment", fscanf(fin, "%m[^\n]", &tok->str));
        }
        // strings
        else if (c == '"') {
            tok = token_next(tok, STRING);
            check_scanf(fin, "unclosed string", fscanf(fin, "%m[^\"\n]\"", &tok->str));
        }
        // macro def
        else if (c == '%') {
            tok = token_next(tok, MACRO);
            if (macro != NULL) {
                tok = macro;
                macro = NULL;
            } else {
                macro = tok;
                tok = tok->macro = token_next(NULL, MACRO);
            }
        }
        // macro use
        else if (c == '@') {
            tok = token_next(tok, REF_USE);
            check_scanf(fin, "bad macro", fscanf(fin, LABEL_FMT, &tok->str));
        }

        // hex
        else if (c == '#') {
            unsigned size;
            check_scanf(fin, "bad hex", fscanf(fin, "%[0-9a-fA-F]%n", buf, &size));

            tok = token_next(tok, INTEGER);
            // guess size from length
            if (size >= 8) tok->inttype = U32;
            else if (size >= 4) tok->inttype = U16;
            else               tok->inttype = U8;
            if (fpeekc(fin) == '\'') { tok->stack = RS; fgetc(fin); }
            if (fpeekc(fin) == '/') tok->inttype = parse_inttype(fin);
            tok->integer = strtol(buf, NULL, 16);
        }

        // label
        else if (c == ':') { tok = token_next(tok, LABEL); check_scanf(fin, "bad label", fscanf(fin, LABEL_FMT, &tok->str)); }
        // refs
        else if (c == '&') { tok = token_next(tok, REF_ADDR); check_scanf(fin, "bad & label", fscanf(fin, LABEL_FMT, &tok->str)); }
        else if (c == '?') { tok = token_next(tok, REF_SIZE); check_scanf(fin, "bad ? label", fscanf(fin, LABEL_FMT, &tok->str)); }
        else if (c == '<') { tok = token_next(tok, REF_LOAD); check_scanf(fin, "bad < label", fscanf(fin, LABEL_FMT, &tok->str)); }
        else if (c == '>') { tok = token_next(tok, REF_STORE); check_scanf(fin, "bad > label", fscanf(fin, LABEL_FMT, &tok->str)); }

        else if (c == '!') { tok = token_next(tok, DATA); }

        // opcodes
        else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
            ungetc(c, fin);
            tok = token_next(tok, OPCODE);
            tok->opcode = parse_opcode(fin);
            if (fpeekc(fin) == '\'') { tok->stack = RS; fgetc(fin); }
            if (fpeekc(fin) == '/') tok->inttype = parse_inttype(fin);
        }

        else {
            char msg[64];
            snprintf(msg, 64, "unexpected token '%c'", c);
            error(fin, msg);
        }
    }
}

void generate(FILE* fout, struct Token* start, bool debug) {
    struct Token* tok = start,
                * prev = start,
                * data = NULL,
                tmp = { .type = OPCODE, .opcode = LIT, .next = NULL };
    char msg[64];

    /* pass 1
     * - label/ref validation
     * - macro unpacking
     */
    for (tok = start; tok != NULL; tok = tok->next) {
        switch(tok->type) {
            case LABEL: {
                if ((token_find(start, LABEL, tok->str) != tok) ||
                    (token_find(tok->next, LABEL, tok->str) != NULL)) {
                    snprintf(msg, 64, "multiple '%s' labels", tok->str);
                    error(NULL, msg);
                }
                struct Token* t = tok;
                while (t != NULL && t->type == LABEL) t = t->next;
                tok->ref = t;
                break;
            }
            case REF_ADDR:
            case REF_SIZE:
            case REF_LOAD:
            case REF_STORE:
            case REF_USE:
                tok->ref = token_find(start, LABEL, tok->str);
                if (tok->ref == NULL) {
                    snprintf(msg, 64, "missing label '%s'", tok->str);
                    error(NULL, msg);
                }

                if (tok->type == REF_USE) {
                    if (tok->ref->next == NULL || tok->ref->ref->type != MACRO) {
                        snprintf(msg, 64, "label '%s' is not macro", tok->str);
                        error(NULL, msg);
                    }
                    token_insert(tok->ref->ref->macro, tok, tok->next);
                }
                break;
            default: break;
        }
    }

    /* pass 2
     * - shorthand replacement
     */
    for (prev = (tok = start); tok != NULL; tok = tok->next) {
        switch(tok->type) {
            case STRING:
                if (data == NULL) error(NULL, "PUSH str is unimplemented");
                break;
            case INTEGER:
                if (data == NULL) {
                    tmp.next = NULL; tmp.type = OPCODE; tmp.opcode = LIT; tmp.inttype = tok->inttype; tmp.stack = tok->stack;
                    token_insert(&tmp, prev, tok);
                }
                break;
            case REF_ADDR:
            case REF_SIZE:
                tmp.next = NULL; tmp.type = OPCODE; tmp.opcode = LIT; tmp.inttype = U16; tmp.stack = tok->stack;
                token_insert(&tmp, prev, tok);
                if (tok->type == REF_SIZE && tok->ref->ref == NULL) {
                    snprintf(msg, 64, "label '%s' has no size", tok->str);
                    error(NULL, msg);
                }
                break;
            case REF_LOAD:
            case REF_STORE: {
                if (tok->ref->ref->type != INTEGER) error (NULL, "'<' or '>' only valid for ints");
                struct Token* last = tok->next;

                // addr
                tmp.next = NULL; tmp.inttype = U16;
                tmp.type = REF_ADDR; tmp.ref = tok->ref;
                struct Token* t = token_insert(&tmp, tok, NULL);

                // page
                tmp.type = INTEGER; tmp.integer = 1;
                t = token_insert(&tmp, t, NULL);

                // load|store
                tmp.type = OPCODE; tmp.inttype = tok->ref->ref->inttype;
                tmp.opcode = tok->type == REF_LOAD ? LOAD : STORE;
                token_insert(&tmp, t, last);
                break;
            }
            case DATA:
                if (data == NULL) data = tok; else data = NULL; break;
            default: break;
        }

        prev = tok;
    }

    /* pass 3
     * - sizing
     * - offset
     */
    data = NULL;
    size_t offset = 0;
    for (tok = start; tok != NULL; tok = tok->next) {
        tok->offset = offset;

        // extra checks
        if (data != NULL && tok->type == OPCODE)
            error(NULL, "opcode in data section");


        switch(tok->type) {
            case STRING:
                if (data == NULL) error(NULL, "PUSH str is unimplemented");
                tok->size = strlen(tok->str);
                break;
            case INTEGER:
                tok->size = TypeSize[tok->inttype]; break;
            case OPCODE:
                tok->size = 1; break;
            case REF_ADDR:
            case REF_SIZE:
                tok->inttype = U16;
                tok->size = 2; break;
            case DATA:
                if (data == NULL) data = tok; else data = NULL; break;
            default: break;
        }

        if (tok->type != DATA) offset += tok->size;
        if (data != NULL) data->size += tok->size;
    }

    // TODO: ensure endian-specific writing
    /* pass 4
     * - codegen
     */
    if (debug) fprintf(stderr, "addr\tinstr\t\tdescription\n");
    for (tok = start; tok != NULL; tok = tok->next) {
        if (tok->type == STRING) {
            if (debug) fprintf(stderr, "%04lx:\t\"%s\"\n", ftell(fout), tok->str);
            fwrite(tok->str, 1, strlen(tok->str), fout);
        } else if (tok->type == INTEGER) {
            if (debug) fprintf(stderr, "%04lx:\t%llu/%s\n", ftell(fout), tok->integer, TypeStr[tok->inttype]);
            fwrite(&tok->integer, TypeSize[tok->inttype], 1, fout);
        } else if (tok->type == OPCODE) {
            if (debug) fprintf(stderr, "%04lx:\t%s/%s\n", ftell(fout), OpCodeStr[tok->opcode], TypeStr[tok->inttype]);
            uint8_t instr = gen_instr(tok);
            fwrite(&instr, 1, 1, fout);
        } else if (tok->type == REF_ADDR) {
            if (debug) fprintf(stderr, "%04lx:\t%zd/%s\t\t&%s\n", ftell(fout), tok->ref->offset, TypeStr[tok->inttype], tok->ref->str);
            fwrite(&tok->ref->offset, 2, 1, fout);
        } else if (tok->type == REF_SIZE) {
            if (debug) fprintf(stderr, "%04lx:\t%zd/%s\t\t?%s\n", ftell(fout), tok->ref->ref->size, TypeStr[tok->ref->ref->inttype], tok->ref->str);
            fwrite(&tok->ref->ref->size, 2, 1, fout);
        } else if (tok->type == LABEL) {
            if (debug) fprintf(stderr, "\n\n%s:\n", tok->str);
        } else if (tok->type == COMMENT) {
            if (debug) fprintf(stderr, "%04lx:\t\t\t;%s\n", ftell(fout), tok->str);
        }
    }
}

void help() {
    const char help_str[] = "Usage: asm SRC.nal OUT.nvm debug?\n";
    fprintf(stderr, "%s", help_str);
    exit(1);
}

int main(int argc, char* argv[]) {
    if (argc < 3) help();

    FILE* fin = fopen(argv[1], "r"),
        * fout = fopen(argv[2], "w+");
    if (fin == NULL || fout == NULL) help();

    struct Token* tok = token_next(NULL, NONE);

    parse(fin, tok);
    generate(fout, tok, argc >= 4);

    fclose(fin);
    fclose(fout);
    token_free(tok);

    return 0;
}
