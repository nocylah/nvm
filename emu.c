#include "nvm.h"

const char help_str[] = "Usage: emu FILE.nvm\n";

void scrub(const char* msg) {
    fprintf(stderr, "ERROR: %s\n", msg);
    exit(1);
}

void read_prog(struct NVM* nvm, FILE* file) {
    void* page = nvm_get_page(nvm, 1);
    fread(page, 1, PAGE_SIZE, file);
    if (ferror(file)) scrub("error loading file");
}

void help() {
    fprintf(stderr, "%s", help_str);
    exit(1);
}

int main(int argc, char* argv[]) {
    if (argc != 2) help();

    FILE* fin = fopen(argv[1], "r");
    if (fin == NULL) help();

    struct NVM nvm;
    nvm_init(&nvm);

    read_prog(&nvm, fin);
    fclose(fin);

    nvm_run(&nvm);

    nvm_free(&nvm);
}
