#include "nvm.h"

const char* OpCodeStr[] = {
    "ADD", "SUB", "MUL", "DIV", "MOD",
    "FLIP", "AND", "OR" , "XOR",
    "SHFTL", "SHFTR",
    "EQ", "LT", "GT",
    "LIT", "GIVE", "DROP", "DUP", "SWAP", "ROT", "OVER", "LOAD", "STORE",
    "JUMP", "CALL", "RET",
    "IOSEL", "IN", "OUT",
    "HALT", "NOP",
};

const char* TypeStr[] = {
    "u8", "u16", "u32", "s32",
};
const size_t TypeSize[] = { 1, 2, 4, 4 };

void nvm_free(struct NVM* nvm) {
    assert(nvm);
    if (nvm->mem == NULL) return;

    for (size_t i = 0; i < PAGE_COUNT; i++) {
        if (nvm->mem[i] != NULL) {
            free(nvm->mem[i]);
        }
    }

    free(nvm->mem);
    nvm->mem = NULL;
}

void nvm_init(struct NVM* nvm) {
    assert(nvm);

    // init page table
    nvm->mem = calloc(PAGE_COUNT, sizeof(void*));
    assert(nvm->mem);

    // setup page 0
    void* page = nvm_get_page(nvm, 0x0000);

    nvm->reg_rs = page + RS_MEM;   *nvm->reg_rs = STACK_END - 1;
    nvm->reg_ds = page + DS_MEM;   *nvm->reg_ds = STACK_START;
    nvm->reg_pc = page + PC_MEM;   *nvm->reg_pc = 0;
    nvm->reg_err = page + ERR_MEM; *nvm->reg_err = 0;

    nvm->err_cond = ERROR_NONE;
}

void* nvm_get_page(struct NVM* nvm, uint16_t page) {
    assert(nvm && nvm->mem);
    if (nvm->mem[page] == NULL) {
        nvm->mem[page] = calloc(PAGE_SIZE, 1);
        assert(nvm->mem[page]);
    }
    return nvm->mem[page];
}

void pop(struct NVM* nvm, enum Stack stack, enum Type type, void* dst) {
    if (stack == DS) {
        if (*nvm->reg_ds - TypeSize[type] < STACK_START) {
            nvm->err_cond = ERROR_DS_UNDERFLOW;
            return;
        }
        *nvm->reg_ds -= TypeSize[type];
        memcpy(dst, nvm->mem[0] + *nvm->reg_ds, TypeSize[type]);
    } else {
        if (*nvm->reg_rs + TypeSize[type] >= STACK_END) {
            nvm->err_cond = ERROR_RS_UNDERFLOW;
            return;
        }
        *nvm->reg_rs += TypeSize[type];
        memcpy(dst, nvm->mem[0] + *nvm->reg_rs, TypeSize[type]);
    }
}

void push(struct NVM* nvm, enum Stack stack, enum Type type, void* src) {
    if (stack == DS) {
        if (*nvm->reg_ds + TypeSize[type] >= *nvm->reg_rs) {
            nvm->err_cond = ERROR_DS_OVERFLOW;
            return;
        }
        memcpy(nvm->mem[0] + *nvm->reg_ds, src, TypeSize[type]);
        *nvm->reg_ds += TypeSize[type];
    } else {
        if (*nvm->reg_rs + TypeSize[type] <= *nvm->reg_ds) {
            nvm->err_cond = ERROR_RS_OVERFLOW;
            return;
        }
        memcpy(nvm->mem[0] + *nvm->reg_rs, src, TypeSize[type]);
        *nvm->reg_rs -= TypeSize[type];
    }
}

// TODO: should write/read only little-endian bytecode
// TODO: reads/writes should handle page boundaries gracefully
void load(struct NVM* nvm, enum Stack stack, enum Type type, uint16_t index,
          uint16_t addr) {
    if ((PAGE_SIZE - addr) < TypeSize[type]) nvm->err_cond = ERROR_MEM_ACCESS;
    void* page = nvm_get_page(nvm, index);
    push(nvm, stack, type, page + addr);
}

void store(struct NVM* nvm, enum Stack stack, enum Type type, uint16_t index,
           uint16_t addr) {
    if ((PAGE_SIZE - addr) < TypeSize[type]) nvm->err_cond = ERROR_MEM_ACCESS;
    void* page = nvm_get_page(nvm, index);
    pop(nvm, stack, type, page + addr);
}

#define BINARY_OP(nvm, stack, type, reg_1, reg_2, reg_3, op, zero_check) \
    pop(nvm, stack, type, reg_3); \
    pop(nvm, stack, type, reg_2); \
    if (zero_check && is_zero(reg_3, type)) { \
            nvm->err_cond = ERROR_ZERO_DIV; \
            break; \
    } \
    switch (type) { \
        case U8: *(uint8_t*)reg_1 = *(uint8_t*)reg_2 op *(uint8_t*)reg_3; break; \
        case U16: *(uint16_t*)reg_1 = *(uint16_t*)reg_2 op *(uint16_t*)reg_3; break; \
        case U32: *(uint32_t*)reg_1 = *(uint32_t*)reg_2 op *(uint32_t*)reg_3; break; \
        case S32: *(int32_t*)reg_1 = *(int32_t*)reg_2 op *(int32_t*)reg_3; break; \
    } \
    push(nvm, stack, type, reg_1);

#define UNARY_OP(nvm, stack, type, reg_1, reg_2, op) \
    pop(nvm, stack, type, reg_2); \
    switch (type) { \
        case U8: *(uint8_t*)reg_1 = op *(uint8_t*)reg_2; break; \
        case U16: *(uint16_t*)reg_1 = op *(uint16_t*)reg_2; break; \
        case U32: *(uint32_t*)reg_1 = op *(uint32_t*)reg_2; break; \
        case S32: *(int32_t*)reg_1 = op *(int32_t*)reg_2; break; \
    } \
    push(nvm, stack, type, reg_1);

#define LOGIC_OP(nvm, stack, type, reg_1, reg_2, reg_3, op) \
    pop(nvm, stack, type, reg_3); \
    pop(nvm, stack, type, reg_2); \
    switch (type) { \
        case U8: *(uint8_t*)reg_1 = *(uint8_t*)reg_2 op *(uint8_t*)reg_3; break; \
        case U16: *(uint8_t*)reg_1 = *(uint16_t*)reg_2 op *(uint16_t*)reg_3; break; \
        case U32: *(uint8_t*)reg_1 = *(uint32_t*)reg_2 op *(uint32_t*)reg_3; break; \
        case S32: *(uint8_t*)reg_1 = *(int32_t*)reg_2 op *(int32_t*)reg_3; break; \
    } \
    push(nvm, stack, U8, reg_1);

bool is_zero(void* num, enum Type type) {
    switch (type) {
        case U8: return *(uint8_t*)num == 0;
        case U16: return *(uint16_t*)num == 0;
        case U32: return *(uint32_t*)num == 0;
        case S32: return *(int32_t*)num == 0;
    }
}

void nvm_run(struct NVM* nvm) {
    uint8_t* code = nvm_get_page(nvm, 1);
    bool GO = true;

    uint8_t reg_a[8] = {},
         reg_b[8] = {},
         reg_c[8] = {};

    FILE* io_in = NULL,
        * io_out = NULL;

    while (GO) {
        if (nvm->err_cond != ERROR_NONE) {
            // goto err handler
            *nvm->reg_pc = *nvm->reg_err;
            // push error
            *(uint8_t*)(nvm->mem[0] + *nvm->reg_ds) = nvm->err_cond;
            if (*nvm->reg_ds < *nvm->reg_rs) *nvm->reg_ds += 1;
            // reset error condition
            nvm->err_cond = ERROR_NONE;
        }

        const uint8_t opcode = *(code + *nvm->reg_pc);
        const uint8_t op = opcode & 0x1F;
        const uint8_t type = (opcode & 0xc0) >> 6;
        const uint8_t stack = (opcode & 0x20) >> 5;

        switch (op) {
            case ADD: BINARY_OP(nvm, stack, type, reg_c, reg_a, reg_b, +, false); break;
            case SUB: BINARY_OP(nvm, stack, type, reg_c, reg_a, reg_b, -, false); break;
            case MUL: BINARY_OP(nvm, stack, type, reg_c, reg_a, reg_b, *, false); break;
            case DIV: BINARY_OP(nvm, stack, type, reg_c, reg_a, reg_b, /, true); break;
            case MOD: BINARY_OP(nvm, stack, type, reg_c, reg_a, reg_b, %, true); break;

            case FLIP: UNARY_OP(nvm, stack, type, reg_c, reg_a, ~); break;
            case AND: BINARY_OP(nvm, stack, type, reg_c, reg_a, reg_b, &, false); break;
            case OR:  BINARY_OP(nvm, stack, type, reg_c, reg_a, reg_b, |, false); break;
            case XOR: BINARY_OP(nvm, stack, type, reg_c, reg_a, reg_b, ^, false); break;

            case SHFTL: BINARY_OP(nvm, stack, type, reg_c, reg_a, reg_b, <<, false); break;
            case SHFTR: BINARY_OP(nvm, stack, type, reg_c, reg_a, reg_b, >>, false); break;

            case EQ: LOGIC_OP(nvm, stack, type, reg_c, reg_a, reg_b, ==); break;
            case LT: LOGIC_OP(nvm, stack, type, reg_c, reg_a, reg_b, <); break;
            case GT: LOGIC_OP(nvm, stack, type, reg_c, reg_a, reg_b, >); break;

            case LIT:
                push(nvm, stack, type, code + *nvm->reg_pc + 1);
                *nvm->reg_pc = *nvm->reg_pc + TypeSize[type] + 1;
                continue;
            case GIVE:
                pop(nvm, stack, type, reg_a);
                push(nvm, !stack, type, reg_a);
                break;
            case DROP:
                pop(nvm, stack, type, reg_a);
                break;
            case DUP:
                pop(nvm, stack, type, reg_a);
                push(nvm, stack, type, reg_a);
                push(nvm, stack, type, reg_a);
                break;
            case SWAP:
                pop(nvm, stack, type, reg_a);
                pop(nvm, stack, type, reg_b);
                push(nvm, stack, type, reg_a);
                push(nvm, stack, type, reg_b);
                break;
            case ROT:
                pop(nvm, stack, type, reg_a);
                pop(nvm, stack, type, reg_b);
                pop(nvm, stack, type, reg_c);
                push(nvm, stack, type, reg_b);
                push(nvm, stack, type, reg_c);
                push(nvm, stack, type, reg_a);
                break;
            case OVER:
                pop(nvm, stack, type, reg_a);
                pop(nvm, stack, type, reg_b);
                push(nvm, stack, type, reg_b);
                push(nvm, stack, type, reg_a);
                push(nvm, stack, type, reg_b);
                break;
            case LOAD:
                // TODO: make u8
                pop(nvm, stack, U16, reg_a);
                pop(nvm, stack, U16, reg_b);
                load(nvm, stack, type, *(uint16_t*)reg_a, *(uint16_t*)reg_b);
                break;
            case STORE:
                pop(nvm, stack, U16, reg_a);
                pop(nvm, stack, U16, reg_b);
                store(nvm, stack, type, *(uint16_t*)reg_a, *(uint16_t*)reg_b);
                break;

            case JUMP:
                pop(nvm, stack, U16, reg_a); // pop destination
                pop(nvm, stack, U8, reg_b);  // pop condition
                if (*reg_b != false) {
                    *nvm->reg_pc = *(uint16_t*)reg_a;
                    continue;
                }
                break;
            case CALL:
                push(nvm, RS, U16, nvm->reg_pc);
                pop(nvm, DS, U16, nvm->reg_pc);
                continue;
            case RET:
                pop(nvm, RS, U16, nvm->reg_pc);
                break;

            case IOSEL:
                pop(nvm, stack, U8, reg_a);
                switch (*reg_a) {
                    case 0:
                        io_in = stdin;
                        io_out = stdout;
                        break;
                    default: io_in = io_out = NULL; break;
                }
                break;
            // TODO: non-blocking io
            case IN:
                if (io_in != NULL) {
                    short c = fgetc(io_in);
                    if (c != EOF) {
                        push(nvm, stack, U8, &c);
                        break;
                    }
                    nvm->err_cond = feof(io_in) ? ERROR_IO_EMPTY : ERROR_IO_COMM;
                } else {
                    nvm->err_cond = ERROR_IO_BADSEL;
                }
                continue;
            case OUT:
                if (io_out != NULL) {
                    pop(nvm, stack, U8, reg_a);
                    if (fputc(*reg_a, io_out) != EOF)
                        break;
                    nvm->err_cond = ERROR_IO_COMM;
                } else {
                    nvm->err_cond = ERROR_IO_BADSEL;
                }
                continue;
            case HALT:
                GO = false;
                break;
            case NOP:
                break;
            default:
                nvm->err_cond = ERROR_OPCODE;
        }
        *nvm->reg_pc = *nvm->reg_pc + 1;
    }
}
