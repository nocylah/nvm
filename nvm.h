#ifndef NVM_H
#define NVM_H

// C89
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

// C99
#include <stdbool.h>
#include <stdint.h>

#define STACK_START 0x0100
#define STACK_END 0xFFFF
#define PAGE_SIZE 65536
#define PAGE_COUNT 65536

enum OpCode {
    ADD = 0, SUB, MUL, DIV, MOD,
    FLIP, AND, OR , XOR,
    SHFTL, SHFTR,
    EQ, LT, GT,
    LIT, GIVE, DROP, DUP, SWAP, ROT, OVER, LOAD, STORE,
    JUMP, CALL, RET,
    IOSEL, IN, OUT,
    HALT, NOP,
};

extern const char* OpCodeStr[];

enum Stack { DS = 0, RS };

enum Type { U8 = 0, U16, U32, S32 };
extern const char* TypeStr[];
extern const size_t TypeSize[];

enum Error {
    ERROR_NONE = 0,
    ERROR_OPCODE,
    ERROR_ZERO_DIV,
    ERROR_RS_UNDERFLOW,
    ERROR_RS_OVERFLOW,
    ERROR_DS_UNDERFLOW,
    ERROR_DS_OVERFLOW,
    ERROR_MEM_ACCESS,
    ERROR_IO_AGAIN,
    ERROR_IO_EMPTY,
    ERROR_IO_BADSEL,
    ERROR_IO_COMM,
};

#define RS_MEM 0
#define DS_MEM 2
#define PC_MEM 4
#define ERR_MEM 6

struct NVM {
    void** mem;
    uint16_t* reg_rs; // return stack top
    uint16_t* reg_ds; // data stack top

    uint16_t* reg_pc; // program counter
    uint16_t* reg_err; // error handler

    // TODO: should this be in page 0?
    enum Error err_cond; // error flag
};

void nvm_init(struct NVM *nmv);
void nvm_free(struct NVM *nmv);
void* nvm_get_page(struct NVM* nvm, uint16_t page);
void nvm_run(struct NVM *nmv);

#endif // NVM_H
