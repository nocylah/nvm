NVM
================================================================================

VM design
--------------------------------------------------------------------------------
- Word size: 8bit.
- Little endian.
- 8-32bit capable.
- 32bit address space.
- 64KiB page table.

Memory Layout
--------------------------------------------------------------------------------

                Main Memory
 0x0000 0x0000 ┏━━━━━━━━━━━━━━━━━━━┓
               ┃  Registers        ┃
               ┃  - RS Pointer     ┃
               ┃  - DS Pointer     ┃
               ┃  - Intr Pointer   ┃
               ┃  - Error Pointer  ┃
 0x0000 0x0100 ┠╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┨
               ┃  Data Stack     ↓ ┃
               ┠╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┨
               ┃  Return Stack   ↑ ┃
 0x0001 0x0000 ┠╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┨
               ┃  Program          ┃
 0x0002 0x0000 ┠╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┨
               ┃  Free             ┃
 0x00ff 0xffff ┗━━━━━━━━━━━━━━━━━━━┛

          Data Stack          Return Stack
         ┏━━━━━━━━━┓         ┏━━━┓
Frame 0  ┃  Arg 1  y         ┃  ←╂──────── Stack Pointer
         ┃   ...   ┃  call2←─╂ * ┃
         ┃  Arg N  ┃  call1←─╂ * ┃
         ┃  Var 1  ┃  call0←─╂ * ┃
         ┃   ...   ┃         ┗━━━┛
         ┃  Var N  ┃
         ┠╌╌╌╌╌╌╌╌╌┨
Frame 1  ┃  Args   ┃
         ┃  Vars   ┃
         ┠╌╌╌╌╌╌╌╌╌┨
Frame 2  ┃  Args   ┃
         ┃  Vars   ┃
         ┃        ←╂──────── Stack Pointer
         ┗━━━━━━━━━┛

Opcodes
--------------------------------------------------------------------------------
bit  0        7
    ┏━━┯━┯━━━━━┓
    ┃ZZ│Y│XXXXX┃
    ┗━━┷━┷━━━━━┛

Z = type - 0:u8 1:u16 2:u32 3:s32
Y = stack - 0:data 1:return
X = operation

┏━━━━━━━┯━━━━━━┯━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃ Instr │ Op   │ Behavior                                                ┃
┣━━━━━━━┿━━━━━━┿━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃ ADD   │ 0x00 │ [a, b] ADD -> [c], c = a + b                            ┃
┃ SUB   │ 0x01 │ [a, b] SUB -> [c], c = a - b                            ┃
┃ MUL   │ 0x02 │ [a, b] MUL -> [c], c = a * b                            ┃
┃ DIV   │ 0x03 │ [a, b] DIV -> [c], c = a / b                            ┃
┃ MOD   │ 0x04 │ [a, b] MOD -> [c], c = a % b                            ┃
┠───────┼──────┼─────────────────────────────────────────────────────────┨
┃ FLIP  │ 0x05 │ [a] FLIP -> [c], c = ~a                                 ┃
┃ AND   │ 0x06 │ [a, b] AND -> [c], c = a & b                            ┃
┃ OR    │ 0x07 │ [a, b] OR  -> [c], c = a | b                            ┃
┃ XOR   │ 0x08 │ [a, b] XOR -> [c], c = a ^ b                            ┃
┠───────┼──────┼─────────────────────────────────────────────────────────┨
┃ SHFTL │ 0x09 │ [a] SHFTL -> [c], c = a << 1 (using C conventions)      ┃
┃ SHFTR │ 0x0A │ [a] SHFTR -> [c], c = a >> 1 (using C conventions)      ┃
┠───────┼──────┼─────────────────────────────────────────────────────────┨
┃ EQ    │ 0x0B │ [a, b] EQ -> [c/u8], c = a == b                         ┃
┃ LT    │ 0x0C │ [a, b] LT -> [c/u8], c = a < b                          ┃
┃ GT    │ 0x1D │ [a, b] GT -> [c/u8], c = a > b                          ┃
┠───────┼──────┼─────────────────────────────────────────────────────────┨
┃ LIT   │ 0x0E │ [] LIT -> [c], 'c' is what follows lit                  ┃
┃ GIVE  │ 0x0F │ [a][] GIVE -> [][a], gives value to other stack         ┃
┃ DROP  │ 0x10 │ [a] DROP -> []                                          ┃
┃ DUP   │ 0x11 │ [a] DUP -> [a, a]                                       ┃
┃ SWAP  │ 0x12 │ [a, b] SWAP -> [b, a]                                   ┃
┃ ROT   │ 0x13 │ [a, b, c] ROT -> [b, c, a]                              ┃
┃ OVER  │ 0x14 │ [a, b] OVER -> [a, b, a]                                ┃
┃ LOAD  │ 0x15 │ [addr/u16, page/u16] LOAD -> [c], c = mem[page][addr]   ┃
┃ STORE │ 0x16 │ [c, addr/u16, page/u16] LOAD -> [], mem[page][addr] = c ┃
┠───────┼──────┼─────────────────────────────────────────────────────────┨
┃ JUMP  │ 0x17 │ [cond/u8, addr/u16] JUMP -> [], pc = addr if cond       ┃
┃ CALL  │ 0x18 │ DS[addr/u16]RS[] CALL -> DS[]RS[addr/u16], pc = addr    ┃
┃ RET   │ 0x19 │ DS[]RS[addr/u16] RET -> DS[]RS[], pc = rs_pop();        ┃
┠───────┼──────┼─────────────────────────────────────────────────────────┨
┃ IOSEL │ 0x1A │ [a/u8] IOSEL -> [], use ioport a                        ┃
┃ IN    │ 0x1B │ [] IN -> [c/u8]                                         ┃
┃ OUT   │ 0x1C │ [a/u8] OUT -> []                                        ┃
┠───────┼──────┼─────────────────────────────────────────────────────────┨
┃ HALT  │ 0x1D │ [] HALT -> [], halts VM                                 ┃
┃ NOP   │ 0x1E │ [] NOP -> []                                            ┃
┃       │ 0x1F │                                                         ┃
┗━━━━━━━┷━━━━━━┷━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

- Jump/Call offsets are absolute.
- Load/Store addresses are absolute.

Refs
--------------------------------------------------------------------------------
- Stack Computers, The New Age: https://users.ece.cmu.edu/~koopman/stack_computers/index.html
- Inspired by Uxn: https://wiki.xxiivv.com/site/uxn.html
