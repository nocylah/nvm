if exists("b:current_syntax")
  finish
endif

let id='[a-zA-Z0-9_.-]\+'
exec "syn match id contained '" . id . "'"

syn match opcodeSize '\/[us]\(8\|16\|32\|64\)'

syn keyword opcode ADD SUB MUL DIV MOD
syn keyword opcode FLIP AND OR  XOR
syn keyword opcode SHFTL SHFTR
syn keyword opcode EQ LT GT
syn keyword opcode LIT GIVE DROP DUP SWAP ROT OVER LOAD STORE
syn keyword opcode JUMP CALL RET
syn keyword opcode IOSEL IN OUT
syn keyword opcode HALT NOP nextgroup=opcodeSize

syn match string '"[^"]*"'

syn match hex '#[0-9a-fA-F]\+'
exe "syn match gLabel '^:" . id . "'"
exe "syn match lLabel '.:" . id . "'"
syn region macroDef start='%' end='%'
syn match macroUse '@' nextgroup=id
syn match ref '[&?<>]' nextgroup=id
syn match data '!'
syn match comment ';.*$'

let b:current_syntax = "nvm"

"hi def link id Identifier
hi def link opcodeSize Type
hi def link opcode Keyword
hi def link string String
hi def link hex Number
hi def link gLabel Function
hi def link lLabel Label
hi def link macroDef Macro
hi def link macroUse Special
hi def link ref Special
hi def link data Constant
hi def link comment Comment
